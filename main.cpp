/**
 * Copyright (C) 2020  Gaurav Mishra
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "prime.hpp"
#include <iostream>

int main()
{
    PrimeDetector p;
    std::string inputJson;
    std::cout << "Hello, please enter a number in following format\n";
    std::cout << "{\"number\": <number>}: ";
    std::getline(std::cin, inputJson);
    std::cout << std::endl;
    Json::Value input = p.readJson(inputJson);
    Json::Value output = p.isPrime(input);
    std::cout << p.printJson(output) << std::endl;
    return 0;
}
