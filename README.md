# DevOps Demo

This is a simple project setup to show how DevOps can help by running test
cases over a CI pipeline to insure integrity of the code.

Also, the project unviels the novelty containerization can bring for delivery
of tool with all the dependencies prebaked.

## Requirements

The tool is designed to run Debian based distros.

### Build dependencies

```shell
sudo apt update && sudo apt install make gcc libjsoncpp-dev
```

### Test dependencies

In addition to build dependencies, test requires CPPUnit.

```shell
sudo apt update && sudo apt install libcppunit-dev
```

## Building

Building the tool is pretty simple. Just run following command (after
installing all the dependencies) from top level folder:

```shell
make
```

## Testing

Once installed all the dependencies, unit test cases can be initiated by:

```shell
make test
```

## Docker

The tool can be containerized using Docker. There are two different Dockerfile
shipped.

The `Dockerfile` uses `debian:buster-slim` as the base image to run the
compiled binary on.

The `Dockerfile.static` uses `scratch` to run the statically compiled binary
resulting in an even compact image.

### Building Debian image

```shell
docker build -t devops-demo:latest -f Dockerfile .
```

### Building scratch image

```shell
docker build -t devops-demo:scratch -f Dockerfile.static .
```

### Pull

- Pulling Debian based image:

```shell
docker pull registry.gitlab.com/gmishx/devops-demo:master
```

- Pulling scratch image:

```shell
docker pull registry.gitlab.com/gmishx/devops-demo:static
```

### Running

To run both the image, simply use following command (change the image tag
accordingly).

```shell
docker run -it --rm devops-demo:<tag>
```
