/**
 * Copyright (C) 2020  Gaurav Mishra
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "prime.hpp"

Json::Value PrimeDetector::readJson(std::string input)
{
    Json::Value root;
    Json::Reader reader;
    bool parsingSuccessful = reader.parse(input, root);
    if (!parsingSuccessful)
    {
        std::cerr << "Error parsing the string" << std::endl;
        exit(-1);
    }
    return root;
}

std::string PrimeDetector::printJson(Json::Value root)
{
    Json::StyledWriter writer;
    return writer.write(root);
}

Json::Value PrimeDetector::isPrime(Json::Value input)
{
    Json::Value root;
    bool prime = true;
    int number = input["number"].asInt();
    for (int i = 2; i < number; i++)
    {
        if (number % i == 0)
        {
            prime = false;
            break;
        }
    }
    root["isPrime"] = prime;
    root["number"] = number;
    return root;
}
